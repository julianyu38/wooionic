22 September 2017
    Fixed a bug with the Account Page Modal
    Refactored the code for Variations
    Fixed bug with variations, Now attributes can have special characters
    Updated some sections to latest API
    Fixed a bug with the login page

23 August 2017
    Added Account Page
    Capability to Edit Addresses on Account Page
    Added Grid View Option on Home and Category Page
    Fixed paging bug in the Orders Page
    Added Firebase Remote Config for Banner Images

27 July 2017
    Fixed clear Cart Bug after Order Placement
    Fixed category names to show HTML
    Prevent order placement with no payment mode selected
    Fixed a bug related to variable products where no price was displayed
    Added checks for display HTML price when variations are found
    Discovered that no attribute terms should have spaces
    Added Autohide Directive for hiding toolbars on scroll

15 July 2017
    Added Parallex Effect on Category Pages using Directive

13 July 2017
    Added Password Retrieve Option on Login Page

12 July 2017
    Added PayUMoney
    Added Push Notification with additionalData

10 July 2017
    Added OneSignal Push Notifications
    Fixed a bug in product search

23 June 2017
    Added Facebook Login
    Dynamic Currency Symbol
    Added badge to show cart count
    Fixed a bug in homepage slider
    Improvement in homepage using viewPerPage in slider

17 June 2017
    Added Lazy Loading for all pages.
    Added direction (rtl/ltr) in settings.
    Fixed a bug where all orders were displayed.

26 May 2017
    Initial Release