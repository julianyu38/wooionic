import { Directive, ElementRef, Renderer, Input } from '@angular/core';

@Directive({
  selector: '[hide-header]',
  host: {
    '(ionScroll)': 'onContentScroll($event)'
  }
})
export class HideHeaderDirective {

  @Input('header') header: HTMLElement;
  headerToHide;
  totalHeight;
  headerHeight;
  scrollContent;
  fixedContent;
  toolbarExists: boolean = true;

  constructor(public element: ElementRef, public renderer: Renderer) {

  }

  ngOnInit() {
    this.totalHeight = this.header.clientHeight;
    this.headerToHide = this.header.getElementsByTagName("ion-toolbar")[0] || this.header;
    this.headerHeight = this.headerToHide.clientHeight;

    if(this.headerHeight == this.totalHeight)
      this.toolbarExists = false;
    //console.log(this.headerHeight + ", " + this.totalHeight)
    this.renderer.setElementStyle(this.header, 'webkitTransition', 'height 500ms');
    this.renderer.setElementStyle(this.header, 'webkitTransitionTimingFunction', 'ease-in-out'); 
    
    this.renderer.setElementStyle(this.headerToHide, 'webkitTransition', 'top 500ms');

    if(this.toolbarExists){
       this.renderer.setElementStyle(this.headerToHide, 'z-index', '-999');
    }
    this.renderer.setElementStyle(this.headerToHide, 'webkitTransitionTimingFunction', 'ease-in-out'); 
       
       
    this.scrollContent = this.element.nativeElement.getElementsByClassName("scroll-content")[0];
    this.fixedContent = this.element.nativeElement.getElementsByClassName("fixed-content")[0];
    this.renderer.setElementStyle(this.scrollContent, 'webkitTransition', 'margin-top 500ms');
    this.renderer.setElementStyle(this.scrollContent, 'webkitTransitionTimingFunction', 'ease-in-out');    
    this.renderer.setElementStyle(this.fixedContent, 'webkitTransition', 'margin-top 500ms');
    this.renderer.setElementStyle(this.fixedContent, 'webkitTransitionTimingFunction', 'ease-in-out');    
  }

  onContentScroll(event) {

    // event.domWrite(() => {
    //   this.renderer.setElementStyle(this.scrollContent, "margin-top", -(event.scrollTop - this.headerHeight) + "px")
    //   console.log(-(event.scrollTop - this.headerHeight))
    // })

    // if (event.scrollTop == this.headerHeight) {
    //   event.domWrite(() => {
    //     this.renderer.setElementStyle(this.header, "webkitTransform", "translate3d(0, 0, 0)");

    //   });
    // } else if (event.scrollTop > 2 * this.headerHeight) {
    //   event.domWrite(() => {
    //     this.renderer.setElementStyle(this.header, "webkitTransform", "translate3d(0, -" + (2 * this.headerHeight) + "px, 0)");
    //   });
    // } else if (event.scrollTop > this.headerHeight && event.scrollTop <= 2 * this.headerHeight) {
    //   event.domWrite(() => {
    //     this.renderer.setElementStyle(this.header, "webkitTransform", "translate3d(0, -" + (event.scrollTop - this.headerHeight) + "px, 0)")
    //   });
    // }

    event.domWrite(()=>{
      if(event.scrollTop > this.headerHeight){
        this.renderer.setElementStyle(this.headerToHide, "top", "-" + this.headerHeight + "px");
        if(this.toolbarExists){
          this.renderer.setElementStyle(this.scrollContent, "margin-top", this.headerHeight + "px");
          this.renderer.setElementStyle(this.fixedContent, "margin-top", this.headerHeight + "px");
          this.renderer.setElementStyle(this.header, "height", this.headerHeight + "px");
        } else {
          this.renderer.setElementStyle(this.scrollContent, "margin-top", "0px");
          this.renderer.setElementStyle(this.fixedContent, "margin-top", "0px");
          this.renderer.setElementStyle(this.header, "height", "0px");
        }
        
      } else {
        this.renderer.setElementStyle(this.headerToHide, "top", "0px");
        this.renderer.setElementStyle(this.scrollContent, "margin-top", this.totalHeight + "px");
        this.renderer.setElementStyle(this.fixedContent, "margin-top", this.totalHeight + "px");
        this.renderer.setElementStyle(this.header, "height", this.totalHeight + "px");
      }
    })
  }

}
