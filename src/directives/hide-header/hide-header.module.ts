
import { NgModule } from '@angular/core';
import { HideHeaderDirective } from './hide-header';

@NgModule({
  declarations: [HideHeaderDirective],
  exports: [HideHeaderDirective]
})
export class HideHeaderDirectiveModule {}