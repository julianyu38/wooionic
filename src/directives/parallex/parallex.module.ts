
import { NgModule } from '@angular/core';
import { ParallexDirective } from './parallex';

@NgModule({
  declarations: [ParallexDirective],
  exports: [ParallexDirective]
})
export class ParallexDirectiveModule {}