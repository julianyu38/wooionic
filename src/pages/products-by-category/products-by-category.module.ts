import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProductsByCategory } from './products-by-category';
import { ParallexDirectiveModule } from "../../directives/parallex/parallex.module";
import { HideHeaderDirectiveModule } from "../../directives/hide-header/hide-header.module";

@NgModule({
  declarations: [
    ProductsByCategory,
  ],
  imports: [
    IonicPageModule.forChild(ProductsByCategory),
    ParallexDirectiveModule,
    HideHeaderDirectiveModule
  ],
  exports: [
    ProductsByCategory
  ]
})
export class ProductsByCategoryModule {}
