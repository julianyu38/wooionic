import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, LoadingController } from 'ionic-angular';
import * as WC from 'woocommerce-api';
// import { ProductDetails } from '../product-details/product-details';
import { Settings } from "../../settings";

@IonicPage({})
@Component({
  selector: 'page-products-by-category',
  templateUrl: 'products-by-category.html',
})
export class ProductsByCategory {

  WooCommerce: any;
  products: any[];
  page: number;
  category: any;
  grid_layout: boolean = false;
  currency_symbol: string;

  constructor(public navCtrl: NavController, public navParams: NavParams, private ngZone: NgZone, private loadingController: LoadingController) {

    this.grid_layout = Settings.enable_grid_layout_category;
    this.currency_symbol = Settings.currency_symbol;

    let alert = this.loadingController.create();

    alert.present();

    this.page = 1;
    this.category = this.navParams.get("category");

    this.WooCommerce = WC({
      url: Settings.store_url,
      consumerKey: Settings.consumer_key,
      consumerSecret: Settings.consumer_secret
    });


    this.WooCommerce.getAsync("products?filter[category]=" + this.category.slug).then((data) => {
      console.log(JSON.parse(data.body));
      this.ngZone.run( ()=>{
        this.products = JSON.parse(data.body).products;
        alert.dismiss();
      })
      
    }, (err) => {
      console.log(err)
    })

  }

  loadMoreProducts(event) {
    this.page++;
    console.log("Getting page " + this.page);
    this.WooCommerce.getAsync("products?filter[category]=" + this.category.slug + "&page=" + this.page).then((data) => {
      let temp = (JSON.parse(data.body).products);

      this.products = this.products.concat(JSON.parse(data.body).products)
      console.log(this.products);
      event.complete();

      if (temp.length < 10)
        event.enable(false);
    })
  }

  openProductPage(product){
    this.navCtrl.push('ProductDetails', {"product": product} );
  }

}
