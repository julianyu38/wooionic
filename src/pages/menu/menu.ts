import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, NavParams, ModalController, Events } from 'ionic-angular';
// import { HomePage } from '../home/home';
// import { Signup } from '../signup/signup';
// import { Login } from '../login/login';
import * as WC from 'woocommerce-api';
// import { ProductsByCategory } from '../products-by-category/products-by-category'
import { Storage } from '@ionic/storage';
import { Cart } from '../cart/cart';
import { Settings } from "../../settings";
// import { OrdersPage } from '../orders/orders';

@IonicPage({})
@Component({
  selector: 'page-menu',
  templateUrl: 'menu.html',
})
export class Menu {

  homePage: any;
  WooCommerce: any;
  categories: any[];
  @ViewChild('content') childNavCtrl: NavController;
  loggedIn: boolean;
  user: any;
  
  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage, public modalCtrl: ModalController, private events: Events) {
    this.homePage = 'HomePage';
    this.categories = [];
    this.user = {};

     this.WooCommerce = WC({
      url: Settings.store_url,
      consumerKey: Settings.consumer_key,
      consumerSecret: Settings.consumer_secret
    });

    this.events.subscribe("user:loggedIn", () => {
      this.ionViewDidEnter();
    });


    this.WooCommerce.getAsync("products/categories").then((data) => {
      //console.log(JSON.parse(data.body).product_categories);

      let temp: any[] = JSON.parse(data.body).product_categories;

      for( let i = 0; i < temp.length; i ++){
        if(temp[i].parent == 0){

          if(temp[i].slug == "clothing"){
            temp[i].icon = "shirt";
          }
          if(temp[i].slug == "music"){
            temp[i].icon = "musical-notes";
          }
          if(temp[i].slug == "posters"){
            temp[i].icon = "images";
          }
          if(temp[i].slug == "mobile-accessories"){
            temp[i].icon = "phone-portrait";
          }

          this.categories.push(temp[i]);
          this.categories[this.categories.length - 1].subCategories = [];

          //Now finding all subcategories for a category

          for (var j = 0; j < temp.length; j++) {

            if(temp[i].id == temp[j].parent){
              this.categories[this.categories.length - 1].subCategories.push(temp[j]);
            }
            
          }

        }
      }

      console.log(this.categories);

    }, (err)=> {
      console.log(err)
    })


  }

  ionViewDidEnter() {

    this.storage.ready().then( () => {
      this.storage.get("userLoginInfo").then( (userLoginInfo) => {

        if(userLoginInfo != null){

          console.log("User logged in...");
          this.user = userLoginInfo.user;
          console.log(this.user);
          this.loggedIn = true;
        }
        else {
          console.log("No user found.");
          this.user = {};
          this.loggedIn = false;
        }

      })
    })


  }

  openCategoryPage(category){

    this.childNavCtrl.push('ProductsByCategory', { "category":  category});

  }

  openPage(pageName: string){
    if(pageName == "signup"){
      this.childNavCtrl.push('Signup');
    }
    if(pageName == "login"){
      this.childNavCtrl.push('Login');
    }
    if(pageName == 'logout'){
      this.storage.remove("userLoginInfo").then( () => {
        this.user = {};
        this.loggedIn = false;
        this.events.publish("user:loggedIn");
      })
    }
    if(pageName == 'cart'){
      let modal = this.modalCtrl.create(Cart, { "navCtrl": this.childNavCtrl });
      modal.present();
    }
    if(pageName == 'orders'){
      this.childNavCtrl.push('OrdersPage');
    }

    if(pageName == 'account'){
      this.childNavCtrl.push('AccountPage');
    }

  }

}
