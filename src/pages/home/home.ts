import { Component, ViewChild } from '@angular/core';
import { IonicPage, NavController, Slides, ToastController, ModalController, Events, Platform } from 'ionic-angular';
// import { ProductDetails } from '../product-details/product-details';
import { Settings } from '../../settings';
import { Storage } from '@ionic/storage';

import * as WC from 'woocommerce-api';
// import { SearchPage } from "../search/search";
import { Cart } from "../cart/cart";

import { SettingsProvider } from '../../providers/settings/settings';

@IonicPage({})
@Component({
  selector: 'page-home',
  templateUrl: 'home.html', providers: [SettingsProvider]
})
export class HomePage {

  WooCommerce: any;
  products: any[];
  moreProducts: any[];
  page: number;
  images: string[] = [];
  arrayForStars: any[];
  searchQuery: string = "";
  currency_symbol: string;
  grid_layout: boolean = false;
  cart_count: number = 0;

  @ViewChild('productSlides') productSlides: Slides;

  constructor(public navCtrl: NavController, public toastCtrl: ToastController, private modalCtrl: ModalController, public storage: Storage,
    public events: Events, private settingsProvider: SettingsProvider, public platform: Platform) {

    this.page = 2;
    this.currency_symbol = Settings.currency_symbol;
    this.grid_layout = Settings.enable_grid_layout_home;

    if (Settings.firebase_get_slider_images == true && this.platform.is('cordova')) {

      //Images will be fetched from Firebase remote config field "slider_images"
      this.settingsProvider.getFireBaseRemoteConfig("slider_images")
        .then((urls) => {
          if (urls.length > 0) {
            this.images = urls;
            this.productSlides.update();
            console.log("this.images: " + JSON.stringify(this.images))
          }
        }).catch((err) => {
          console.log(err)
        });


    }

    else {
      //default images with static paths

      this.images.push("http://samarth.southeastasia.cloudapp.azure.com/wp-content/uploads/2017/09/1.jpg");
      this.images.push("http://samarth.southeastasia.cloudapp.azure.com/wp-content/uploads/2017/09/2.jpg");
      this.images.push("http://samarth.southeastasia.cloudapp.azure.com/wp-content/uploads/2017/09/3.jpg");
      this.images.push("http://samarth.southeastasia.cloudapp.azure.com/wp-content/uploads/2017/09/4.jpg");

    }

    this.events.subscribe("updateCart", () => {
      this.updateCart();
    })

    this.WooCommerce = WC({
      url: Settings.store_url,
      consumerKey: Settings.consumer_key,
      consumerSecret: Settings.consumer_secret,
      wpAPI: true,
      version: 'wc/v2'
    });

    this.loadMoreProducts(null);

    this.WooCommerce.getAsync("products").then((data) => {
      console.log(JSON.parse(data.body));
      this.products = JSON.parse(data.body);
    }).catch((err) => {
      alert("There was an error connecting to the server at the moment. We are working on it. Please try again later.")
    })

  }

  ionViewWillEnter() {

  }

  ionViewDidLoad() {
    setInterval(() => {

      if (this.productSlides.getActiveIndex() == this.productSlides.length() - 1)
        this.productSlides.slideTo(0, 2000);

      this.productSlides.slideNext(1000);
    }, 3000);

    this.updateCart();
  }

  updateCart() {
    this.storage.ready().then(() => {

      this.storage.get("cart").then((data) => {
        let cartItems = data;

        if (cartItems && cartItems.length > 0) {

          this.cart_count = cartItems.length

        } else {

          this.cart_count = 0;

        }


      })

    })
  }

  loadMoreProducts(event) {
    //console.log(event);
    if (event == null) {
      this.page = 2;
      this.moreProducts = [];
    }
    else
      this.page++;

    this.WooCommerce.getAsync("products?page=" + this.page).then((data) => {
      console.log(JSON.parse(data.body));
      this.moreProducts = this.moreProducts.concat(JSON.parse(data.body));

      if (event != null) {
        event.complete();
      }

      if (JSON.parse(data.body).length < 10) {
        if (event != null) {
          event.enable(false);
        }

        this.toastCtrl.create({
          message: "No more products!",
          duration: 5000
        }).present();

      }


    }).catch((err) => {
      console.log(err)
    })
  }

  openProductPage(product) {
    this.navCtrl.push('ProductDetails', { "product": product });
  }

  onSearch(event) {
    //console.log(event);
    if (this.searchQuery.length > 1) {
      this.navCtrl.push('SearchPage', { "searchQuery": this.searchQuery })
    }

  }

  openCart() {
    this.modalCtrl.create(Cart, { "navCtrl": this.navCtrl }).present();
  }

}
