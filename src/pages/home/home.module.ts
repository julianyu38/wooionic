import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { HomePage } from './home';
import { HideHeaderDirectiveModule } from "../../directives/hide-header/hide-header.module";
import { SettingsProvider } from '../../providers/settings/settings';

@NgModule({
  declarations: [
    HomePage,
  ],
  imports: [
    IonicPageModule.forChild(HomePage),
    HideHeaderDirectiveModule
  ],
  providers: [SettingsProvider],
  exports: [
    HomePage
  ]
})
export class HomePageModule {}
