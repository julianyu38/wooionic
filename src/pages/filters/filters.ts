import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, ToastController } from 'ionic-angular';
import { Storage } from '@ionic/storage';


@Component({
  selector: 'page-filters',
  templateUrl: 'filters.html',
})
export class FiltersPage {
  category: {};

  productAttributes: any[] = [];
  productCategories: any[] = [];

  filters: any = {}

  constructor(public navCtrl: NavController, public navParams: NavParams, public storage: Storage, public viewController: ViewController, private toastController: ToastController) {

    this.productAttributes = this.navParams.get("product_attributes");
    this.productCategories = this.navParams.get("product_categories");

    for (let i = 0; i < this.productAttributes.length; i++) {

      if (!this.filters[this.productAttributes[i].name]) {
        this.filters[this.productAttributes[i].name] = {};
      }

      for (var j = 0; j < this.productAttributes[i].product_attribute_terms.length; j++) {

        if (!this.filters[this.productAttributes[i].name][this.productAttributes[i].product_attribute_terms[j].slug]) {
          this.filters[this.productAttributes[i].name][this.productAttributes[i].product_attribute_terms[j].slug] = false;

        }
      }
    }

    this.category = {};
    for (let i = 0; i < this.productCategories.length; i++) {
      this.category[this.productCategories[i].slug] = false;
    }

    this.filters.category = this.category;

    this.storage.get("filters").then((result) => {

      if (result) {
        console.log("result", result);
        this.filters = result;
        //this.category = result.category;

        for (var i = 0; i < this.productCategories.length; i++) {

          if (result.category[this.productCategories[i].slug]) {
            this.category[this.productCategories[i].slug] = result.category[this.productCategories[i].slug] || false;
          }

        }

        for (let i = 0; i < this.productAttributes.length; i++) {

          if (!this.filters[this.productAttributes[i].name]) {
            result[this.productAttributes[i].name] = {};
          }

          for (var j = 0; j < this.productAttributes[i].product_attribute_terms.length; j++) {

            if (!this.filters[this.productAttributes[i].name][this.productAttributes[i].product_attribute_terms[j].slug]) {
              this.filters[this.productAttributes[i].name][this.productAttributes[i].product_attribute_terms[j].slug] = result[this.productAttributes[i].name][this.productAttributes[i].product_attribute_terms[j].slug] || false;

            }
          }
        }

      }

      console.log(this.filters);



    })
    console.log("Modal Loaded");

  }

  clearFilters() {
    for (let i = 0; i < this.productAttributes.length; i++) {

      if (!this.filters[this.productAttributes[i].name]) {
        this.filters[this.productAttributes[i].name] = {};
      }

      for (var j = 0; j < this.productAttributes[i].product_attribute_terms.length; j++) {

        if (!this.filters[this.productAttributes[i].name][this.productAttributes[i].product_attribute_terms[j].slug]) {
          this.filters[this.productAttributes[i].name][this.productAttributes[i].product_attribute_terms[j].slug] = false;

        }
      }
    }

    this.category = {};
    for (let i = 0; i < this.productCategories.length; i++) {
      this.category[this.productCategories[i].slug] = false;
    }

    this.filters.category = this.category;

    this.storage.remove("filters").then(() => {
      this.toastController.create({
        message: "Filters Cleared.",
        duration: 2000
      }).present();
    })
  }

  applyFilters() {

    this.filters.category = this.category;
    console.log(this.filters);

    this.storage.set("filters", this.filters).then((value) => {
      console.log(value);

      this.viewController.dismiss(this.filters);
    });
  }

  closeModal(){
    this.viewController.dismiss();
  }

}
