import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { AccountPage } from './account';
import { ParallexDirectiveModule } from "../../directives/parallex/parallex.module";
import { HideHeaderDirectiveModule } from "../../directives/hide-header/hide-header.module";

@NgModule({
  declarations: [
    AccountPage,
  ],
  imports: [
    IonicPageModule.forChild(AccountPage),
    ParallexDirectiveModule,
    HideHeaderDirectiveModule
  ],
  exports: [
    AccountPage
  ]
})
export class AccountPageModule {}
