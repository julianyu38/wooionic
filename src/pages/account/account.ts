import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, LoadingController, ModalController } from 'ionic-angular';
import { Settings } from '../../settings';
import * as WC from 'woocommerce-api';
// import { ProductDetails } from "../product-details/product-details";
import { Storage } from '@ionic/storage';
import { EditAccountModalPage } from "../edit-account-modal/edit-account-modal";


@IonicPage()
@Component({
  selector: 'page-account',
  templateUrl: 'account.html',
})
export class AccountPage {

  WooCommerce: any;
  user_id;
  customer: any = {};

  constructor(public navCtrl: NavController, public navParams: NavParams, private ngZone: NgZone, private loadingController: LoadingController, private toastController: ToastController, public storage: Storage, private modalCtrl: ModalController) {

    this.customer.billing_address = {};
    this.customer.shipping_address = {};

    

    this.WooCommerce = WC({
      url: Settings.store_url,
      consumerKey: Settings.consumer_key,
      consumerSecret: Settings.consumer_secret
    });

    this.getCustomerData();

  }

  getCustomerData(){
    let loading = this.loadingController.create();
    loading.present();
    this.storage.ready().then( () => {
      this.storage.get("userLoginInfo").then( (userLoginInfo) => {
        
        if(userLoginInfo != null){
          this.user_id = userLoginInfo.user.id;
        }
        else {
          console.log("No user found.");
        }

        this.WooCommerce.getAsync("customers/" + this.user_id).then((orderData) => {
          this.ngZone.run(() => {
            this.customer = JSON.parse(orderData.body).customer;
            console.log(this.customer);
            loading.dismiss();
          })

        })

      })
    })
  }

  showEditModal(){
    let modal = this.modalCtrl.create(EditAccountModalPage, {"customer": this.customer});
    modal.onDidDismiss(data => {
     console.log(data);
     if(data.updated == true){
      this.getCustomerData();
     }
   });
    modal.present();
  }


}
