import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams, ToastController, AlertController, Events, LoadingController } from 'ionic-angular';
import { Http } from '@angular/http';
import { Storage } from '@ionic/storage';
import { Settings } from '../../settings';
import { Facebook } from '@ionic-native/facebook';
// import { Signup } from "../signup/signup";
// import { HomePage } from "../home/home";
// import { Menu } from "../menu/menu";

@IonicPage({})
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class Login {

  username: string;
  password: string;
  showFacebookLogin: boolean = false;

  constructor(public navCtrl: NavController, public navParams: NavParams, public http: Http, public toastCtrl: ToastController, public storage: Storage, public alertCtrl: AlertController, private events: Events, private loadingController: LoadingController, private fb: Facebook) {

    this.username = "";
    this.password = "";

    this.showFacebookLogin = Settings.facebook_login;

  }

  login() {

    let loading = this.loadingController.create();
    loading.present();

    this.http.get(Settings.store_url + "/api/auth/generate_auth_cookie/?insecure=cool&username=" + this.username + "&password=" + this.password)
      .subscribe((res) => {
        console.log(res.json());

        let response = res.json();

        if (response.error) {
          this.toastCtrl.create({
            message: response.error,
            duration: 5000
          }).present();

          loading.dismiss();
          return;
        }


        this.storage.set("userLoginInfo", response).then((data) => {

          this.alertCtrl.create({
            title: "Login Successful",
            message: "You have been logged in successfully.",
            buttons: [{
              text: "OK",
              handler: () => {

                this.events.publish("user:loggedIn");
                loading.dismiss();

                if (this.navParams.get("next")) {
                  this.navCtrl.push(this.navParams.get("next"));
                } else {
                  this.navCtrl.setRoot('Menu');
                }
              }
            }]
          }).present();
        })
      }, (err) => {
        loading.dismiss();
        this.toastCtrl.create({
            message: "An error occurred.",
            duration: 5000
          }).present();
      });
  }

  goToSignup() {
    this.navCtrl.push('Signup');
  }

  loginWithFacebook() {
    this.fb.login(["email"]).then((loginResponse) => {
      let loading = this.loadingController.create({
        content: 'Logging in with Facebook'
      });
      loading.present();

      this.http.get(Settings.store_url + "/api/user/fb_connect/?access_token=" + loginResponse.authResponse.accessToken + "&insecure=cool")
        .subscribe((response) => {
          //alert(JSON.stringify(response.json()))

          this.http.get(Settings.store_url + "/api/auth/get_currentuserinfo/?cookie=" + response.json().cookie + "&insecure=cool")
            .subscribe((r) => {

              let response = r.json();

              if (response.error) {
                this.toastCtrl.create({
                  message: response.error,
                  duration: 5000
                }).present();

                loading.dismiss();
                return;
              }

              this.storage.set("userLoginInfo", response).then((data) => {

                this.alertCtrl.create({
                  title: "Login Successful",
                  message: "You have been logged in successfully.",
                  buttons: [{
                    text: "OK",
                    handler: () => {

                      this.events.publish("user:loggedIn");
                      loading.dismiss();

                      if (this.navParams.get("next")) {
                        this.navCtrl.push(this.navParams.get("next"));
                      } else {
                        this.navCtrl.setRoot('Menu');
                      }
                    }
                  }]
                }).present();
              })

            })

        },
        (error) => {
          alert(JSON.stringify(error));
        });
    })
  }

  retrievePassword() {
    let ale = this.alertCtrl.create({
      title: 'Forgot Password',
      message: "We will send you an email with the link to recover/reset your password.",
      inputs: [
        {
          name: 'username',
          placeholder: 'Username'
        }],
      buttons: [
        {
          text: 'Cancel',
          role: 'cancel',
          handler: data => {
            console.log('Cancel clicked');
          }
        },
        {
          text: 'Retrieve',
          handler: data => {
            if(data.username.length > 0){
              this.http.get(Settings.store_url + "/api/user/retrieve_password/?insecure=cool&user_login=" + data.username).subscribe((r) => {
                let res = r.json();

                if(res.status == "error"){
                  alert(res.error);
                }

                if(res.status == "ok"){
                  alert(res.msg);
                }

              })
            }
          }
        }
      ]
    });
    ale.present();
  }

}
