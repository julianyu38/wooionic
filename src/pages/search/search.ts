import { Component, NgZone } from '@angular/core';
import { IonicPage, NavController, NavParams, ActionSheetController, ModalController, LoadingController } from 'ionic-angular';
import { Settings } from '../../settings';
import * as WC from 'woocommerce-api';
// import { ProductDetails } from "../product-details/product-details";
import { FiltersPage } from "../filters/filters";

@IonicPage({})
@Component({
  selector: 'page-search',
  templateUrl: 'search.html',
})
export class SearchPage {

  searchQuery: string = "";
  WooCommerce: any;
  products: any[] = [];
  pageNumber: number = 1;
  sortOrder: string = "desc";
  sortAttribute: string = "total_sales";
  hasMoreData: boolean = true;
  productAttributes: any[] = [];
  productAttributeTerms: any[] = [];
  productCategories: any[] = [];
  url: string = "";
  filterUrl: string = "";

  constructor(public navCtrl: NavController, public navParams: NavParams, private ngZone: NgZone, private actionSheetCtrl: ActionSheetController, private loading: LoadingController, private modalController: ModalController) {

    this.searchQuery = this.navParams.get("searchQuery");

    this.WooCommerce = WC({
      url: Settings.store_url,
      consumerKey: Settings.consumer_key,
      consumerSecret: Settings.consumer_secret,
      // wpAPI: true,
      // version: "wc/v2"
    });

    this.WooCommerce.getAsync("products?filter[q]=" + this.searchQuery + "&filter[order]=" + this.sortOrder + "&filter[orderby]=meta_value_num&filter[orderby_meta_key]=" + this.sortAttribute).then((searchData) => {
      //console.log(searchData)
      this.ngZone.run(() => {
        this.products = JSON.parse(searchData.body).products;
      })

      console.log(this.products);
    })

  }

  showSortOptions() {
    this.actionSheetCtrl.create({
      buttons: [{
        text: "Price - Low to High",
        handler: () => {
          console.log("price low to high");
          this.sortOrder = "asc";
          this.sortAttribute = "_regular_price",
            this.pageNumber = 1;
          this.hasMoreData = true;
          this.sort();
        }
      }, {
        text: "Price - High to Low",
        handler: () => {
          console.log("price high to low");
          this.sortOrder = "desc";
          this.sortAttribute = "_regular_price",
            this.pageNumber = 1;
          this.hasMoreData = true;
          this.sort();
        }
      }, {
        text: "Ratings",
        handler: () => {
          console.log("ratings");
          this.sortOrder = "desc";
          this.sortAttribute = "_wc_average_rating",
            this.pageNumber = 1;
          this.hasMoreData = true;
          this.sort();
        }
      }, {
        text: "Popularity",
        handler: () => {
          console.log("popularity");
          this.sortOrder = "desc";
          this.sortAttribute = "total_sales",
          this.pageNumber = 1;
          this.hasMoreData = true;
          this.sort();
        }
      }]
    }).present();
  }

  sort(event?) {
    let loading = this.loading.create();

    loading.present();
    if(this.filterUrl.length > 0){
      this.url = "products?filter[q]=" + this.searchQuery + "&page=" + this.pageNumber + "&filter[order]=" + this.sortOrder + "&filter[orderby]=meta_value_num&filter[orderby_meta_key]=" + this.sortAttribute;
      this.url = this.url + this.filterUrl;
    } else {
      this.url = "products?filter[q]=" + this.searchQuery + "&page=" + this.pageNumber + "&filter[order]=" + this.sortOrder + "&filter[orderby]=meta_value_num&filter[orderby_meta_key]=" + this.sortAttribute;
    }

    console.log(this.url)

    this.WooCommerce.getAsync(this.url).then((searchData) => {
      //console.log(searchData)
      this.ngZone.run(() => {
        if (this.pageNumber == 1) {
          this.products = JSON.parse(searchData.body).products;
        }
        else
          this.products = this.products.concat(JSON.parse(searchData.body).products);

        if (event) {
          event.complete();
        }
        if (JSON.parse(searchData.body).products.length < 10)
          this.hasMoreData = false;
      })

      console.log(this.products);
      loading.dismiss();
    })
  }

  loadMoreProducts(event) {
    this.pageNumber++;
    this.sort(event);
  }

  openProductPage(product) {
    this.navCtrl.push('ProductDetails', { "product": product });
  }

  async showFilterOptions() {

    let loading = this.loading.create();
    loading.present();
    
    await this.getAttributes();

    await this.getAttributeTerms();

    await this.getCategories();

    let modal = this.modalController.create(FiltersPage, { "product_attributes": this.productAttributes, "product_categories": this.productCategories }, {});

    modal.onDidDismiss((filters) => {

      if(filters){
        this.getFilteredProducts(filters);
      }

    })
    modal.present();

    loading.dismiss();


  }

  async getAttributes() {

    return new Promise<void>((resolve) => {
      this.WooCommerce.getAsync("products/attributes").then((attributesData) => {
        console.log(JSON.parse(attributesData.body));

        this.productAttributes = JSON.parse(attributesData.body).product_attributes;
        resolve();

      })
    });
  }

  async getAttributeTerms() {

    return new Promise<void>((resolve) => {

      for (let i = 0; i < this.productAttributes.length; i++) {

        this.WooCommerce.getAsync("products/attributes/" + this.productAttributes[i].id + "/terms").then((attributeTermsData) => {
          console.log(JSON.parse(attributeTermsData.body).product_attribute_terms);

          this.productAttributes[i].product_attribute_terms = JSON.parse(attributeTermsData.body).product_attribute_terms;

          if (i == this.productAttributes.length - 1)
            resolve();
        })
      }
    });

  }

  async getCategories() {

    return new Promise<void>((resolve) => {
      this.WooCommerce.getAsync("products/categories").then((categoriesData) => {
        console.log(JSON.parse(categoriesData.body));

        this.productCategories = JSON.parse(categoriesData.body).product_categories;
        resolve();

      })
    });
  }

  getFilteredProducts(filters) {

    console.log(filters);

    let url = "";

    for (var attribute in filters) {
      if(attribute == "category"){
        for (var terms in filters[attribute]){
          if(filters[attribute][terms])
            url = url + "&filter[category]=" + terms;
        }
      } else{
        for (var terms in filters[attribute]){
          if(filters[attribute][terms])
            url = url + "&filter[pa_" + attribute + "]=" + terms;
        }
      } 
      
    }

    console.log(url);
    this.filterUrl = url;
    this.sort();

  }

}

