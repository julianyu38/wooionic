import { NgModule } from '@angular/core';
import { IonicPageModule } from 'ionic-angular';
import { ProductDetails } from './product-details';
import { HideHeaderDirectiveModule } from "../../directives/hide-header/hide-header.module";

@NgModule({
  declarations: [
    ProductDetails,
  ],
  imports: [
    IonicPageModule.forChild(ProductDetails),
    HideHeaderDirectiveModule
  ],
  exports: [
    ProductDetails
  ]
})
export class ProductDetailsModule {}
