import { Component } from '@angular/core';
import { NavController, NavParams, ViewController, AlertController, LoadingController, Loading } from 'ionic-angular';
import * as WC from 'woocommerce-api';
import { Http } from '@angular/http';
import { Settings } from "../../settings";

@Component({
  selector: 'page-edit-account-modal',
  templateUrl: 'edit-account-modal.html',
})
export class EditAccountModalPage {

  customer: any = {};
  billing_shipping_same: boolean;
  WooCommerce: any;
  loading: Loading;

  constructor(public navCtrl: NavController, public navParams: NavParams, private viewCtrl: ViewController, private alertCtrl: AlertController, private loadingCtrl: LoadingController, private http: Http) {
    this.customer = this.navParams.get("customer");
    console.log(this.customer)
    this.billing_shipping_same = false; 

    this.loading = this.loadingCtrl.create();
    this.viewCtrl.showBackButton(true);
  }

  setBillingToShipping(){
    this.billing_shipping_same = !this.billing_shipping_same;
  }

  closeModal(){
    this.viewCtrl.dismiss({"updated": false});
  }

  save(){

    if(this.billing_shipping_same == true){
      this.customer.shipping_address = this.customer.billing_address;
    }

    this.loading.present();
    this.updateCustomer();

  //   let aler = this.alertCtrl.create({
  //   title: 'Please enter your password',
  //   inputs: [
  //     {
  //       name: 'password',
  //       placeholder: 'Password',
  //       type: 'password'
  //     }
  //   ],
  //   buttons: [
  //     {
  //       text: 'Cancel',
  //       role: 'cancel',
  //       handler: data => {
  //         console.log('Cancel clicked');
  //       }
  //     },
  //     {
  //       text: 'OK',
  //       handler: async data => {
          
  //         this.loading.present();
  //         await this.verifyPassword(data).then(()=>{
  //           this.updateCustomer();
  //         }).catch((error)=>{
  //           this.loading.dismiss();
  //           alert(error.error);
  //         });
          
  //       }
  //     }
  //   ]
  // });
  // aler.present();
  }

  verifyPassword(data): Promise<any> {
    console.log(data);
    return new Promise<any>((resolve, reject) => {
    this.http.get(Settings.store_url + "/api/auth/generate_auth_cookie/?insecure=cool&username=" + this.customer.username + "&password=" + data.password)
      .subscribe((res) => {
        let response = (res.json());
        console.log(response)

        if(response.status == "error"){
          reject(response);
        } else if(response.user){
          resolve(response);
        }

      }, err => {
        alert("Oops! An error occurred!");
      })
    })
  }

  updateCustomer(){
    this.WooCommerce = WC({
      url: Settings.store_url,
      consumerKey: Settings.consumer_key,
      consumerSecret: Settings.consumer_secret
    });

    this.customer.billing_address.first_name = this.customer.first_name;
    this.customer.billing_address.last_name = this.customer.last_name;

    this.WooCommerce.putAsync("customers/" + this.customer.id, {customer: this.customer}).then((res)=>{
      if(JSON.parse(res.body).customer){
        this.loading.dismiss();
        this.viewCtrl.dismiss({"updated": true});
      }
    }, (err)=>{
      console.log(err.body)
    })
  }

}
