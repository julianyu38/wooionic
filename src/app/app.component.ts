import { Component, ViewChild } from '@angular/core';
import { Nav, Platform, LoadingController } from 'ionic-angular';
import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { OneSignal } from "@ionic-native/onesignal"
import { Settings } from '../settings';
// import { Menu } from '../pages/menu/menu';
// import { OrderPlacedPage } from '../pages/order-placed/order-placed';
import * as WC from 'woocommerce-api';

@Component({
  templateUrl: 'app.html'
})
export class MyApp {
  @ViewChild(Nav) nav: Nav;

  rootPage: any = 'Menu';
  WooCommerce: any;

  constructor(public platform: Platform, public statusBar: StatusBar, public splashScreen: SplashScreen, public oneSignal: OneSignal, public loadingCtrl: LoadingController) {
    this.initializeApp();
    this.platform.setDir(Settings.direction, true);

  }

  initializeApp() {
    this.platform.ready().then(() => {
      // Okay, so the platform is ready and our plugins are available.
      // Here you can do any higher level native things you might need.
      this.statusBar.styleDefault();
      this.statusBar.backgroundColorByHexString("#f53d3d"); //Default set to Danger
      this.splashScreen.hide();
      
      if (Settings.onesignal_enabled && this.platform.is("cordova")) {
        this.initializeOneSignal();
      }



    });
  }

  initializeOneSignal() {

    this.oneSignal.startInit(Settings.onesignal_appId, Settings.google_project_number);

    this.oneSignal.inFocusDisplaying(this.oneSignal.OSInFocusDisplayOption.InAppAlert);

    this.oneSignal.handleNotificationReceived().subscribe((data) => {
      //alert(JSON.stringify(data))
    });

    this.oneSignal.handleNotificationOpened().subscribe((data) => {
      if (data.notification.payload.additionalData) {

        let loading = this.loadingCtrl.create();
        loading.present();

        let additionalData = data.notification.payload.additionalData;
        let product_id = additionalData.product_id;
        let category_id = additionalData.category_id;

        if (product_id) {
          this.WooCommerce = WC({
            url: Settings.store_url,
            consumerKey: Settings.consumer_key,
            consumerSecret: Settings.consumer_secret,
            // wpAPI: true,
            // version: "wc/v2"
          });

          this.WooCommerce.getAsync("products/" + product_id).then((prod) => {
            let product = (JSON.parse(prod.body)).product;

            if (product) {
              loading.dismiss();
              this.nav.getActiveChildNav().push("ProductDetails", { "product": product });
            }

            loading.dismiss();

          })
        } else if (category_id) {

          //alert(category_id)

          this.WooCommerce = WC({
            url: Settings.store_url,
            consumerKey: Settings.consumer_key,
            consumerSecret: Settings.consumer_secret,
            // wpAPI: true,
            // version: "wc/v2"
          });

          this.WooCommerce.getAsync("products/categories/" + category_id).then((cat) => {
            let category = (JSON.parse(cat.body)).product_category;
            //alert(JSON.stringify(cat))
            if (category) {
              loading.dismiss();
              this.nav.getActiveChildNav().push('ProductsByCategory', { "category": category });
            }

            loading.dismiss();

          })
        } else {
          loading.dismiss();
        }

      }
    });

    this.oneSignal.endInit();
  }

}
