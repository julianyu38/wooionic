import { BrowserModule } from '@angular/platform-browser';
import { ErrorHandler, NgModule } from '@angular/core';
import { IonicApp, IonicErrorHandler, IonicModule } from 'ionic-angular';

import { MyApp } from './app.component';
// import { HomePage } from '../pages/home/home';
// import { Menu } from '../pages/menu/menu';
// import { ProductsByCategory } from '../pages/products-by-category/products-by-category';
// import { ProductDetails } from '../pages/product-details/product-details';
import { Cart } from '../pages/cart/cart';
import { EditAccountModalPage } from '../pages/edit-account-modal/edit-account-modal';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
// import { Signup } from '../pages/signup/signup';
// import { Login } from '../pages/login/login';
// import { Checkout } from '../pages/checkout/checkout';
import { HttpModule } from '@angular/http';
import { PayPal } from '@ionic-native/paypal';
// import { OrdersPage } from '../pages/orders/orders';
// import { SearchPage } from '../pages/search/search';
import { FiltersPage } from '../pages/filters/filters';
// import { OrderPlacedPage } from '../pages/order-placed/order-placed';
import { Facebook } from '@ionic-native/facebook';
import { IonicStorageModule } from '@ionic/storage';
import { PhotoViewer } from '@ionic-native/photo-viewer';
import { OneSignal } from '@ionic-native/onesignal';
import { InAppBrowser } from '@ionic-native/in-app-browser';


@NgModule({
  declarations: [
    MyApp,
    Cart,
    FiltersPage,
    EditAccountModalPage
  ],
  imports: [
    BrowserModule,
    HttpModule,
    IonicModule.forRoot(MyApp),
    IonicStorageModule.forRoot()
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    Cart,
    FiltersPage,
    EditAccountModalPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    PayPal,
    PhotoViewer,
    Facebook,
    OneSignal,
    InAppBrowser,
    {provide: ErrorHandler, useClass: IonicErrorHandler}
  ]
})
export class AppModule {}
